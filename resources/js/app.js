
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const Home = require('./components/Home.vue').default

const Articles = require('./components/Articles.vue').default


Vue.component(
    'home', 
    Home
);

Vue.component(
    'carousel', 
    require('./components/Carousel.vue').default
);

Vue.component(
    'articles', 
    Articles
);




const routes = [
    {
        path: '/',
        redirect: '/articles',
        name: 'home',
        component: Home,
        children: [
          {
            path: '/articles',
            name: 'articles',
            component:Articles
          }]
      }
    
  ]
  
  // 3. Создаём экземпляр маршрутизатора и передаём маршруты в опции `routes`
  // Вы можете передавать и дополнительные опции, но пока не будем усложнять.
  const router = new VueRouter({
    mode: 'history',
    routes // сокращённая запись для `routes: routes`
  })

const app = new Vue({
    el:'#app',
    router
  })
