-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Сен 21 2018 г., 19:12
-- Версия сервера: 10.1.35-MariaDB
-- Версия PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `larticles`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Non tempore blanditiis doloremque rem.', 'Corporis facilis eligendi quia aut. Vel consectetur ut iste eum consequuntur dolorum. Omnis accusamus velit sit ab perspiciatis.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(2, 'Nesciunt quod sequi deleniti quis.', 'Ea et excepturi velit maiores esse unde. Asperiores sit illum voluptatem soluta repudiandae vitae. Laboriosam itaque dolorem pariatur illum ex corrupti dolores alias. Aspernatur sapiente fugit est.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(4, 'Error quia ullam et quaerat.', 'Et autem quos placeat. Sint maiores et et impedit dolores voluptates dolorum. Voluptatem dicta velit in quaerat. Sint fugiat maiores eligendi neque.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(5, 'test title21123123123123123123', 'test body', '2018-09-08 10:14:43', '2018-09-11 13:33:55'),
(6, 'Vero eveniet voluptas asperiores hic.', 'Delectus sed aut deleniti quae delectus debitis dolorum animi. Temporibus voluptates vel autem. Voluptatum nemo minus animi perspiciatis rerum porro consequatur. Eum maiores et quis perspiciatis.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(7, 'Commodi amet molestiae ut id.', 'Consectetur quo commodi hic molestiae voluptatum provident. At quis eos ratione omnis perferendis sed. Fugit magnam error nam quis omnis et. Ipsum quam exercitationem rerum quibusdam.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(8, 'Dolorum possimus atque id deleniti delectus.', 'Ut nulla nulla voluptatem aut sapiente doloremque. Suscipit mollitia qui placeat vero porro. Nam voluptatem hic a et reiciendis. Velit nihil quis perferendis harum nihil aut.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(9, 'Omnis labore odio et ea enim.', 'Suscipit sed enim beatae quaerat ratione voluptate hic ut. Laboriosam itaque repellendus harum aut. Temporibus dolorem quaerat maiores odit provident qui quos libero.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(10, 'Veniam et earum velit quo provident maiores.', 'Sit architecto porro laudantium et omnis sunt consequuntur. Necessitatibus ratione perferendis assumenda omnis. Dolores impedit eveniet iure exercitationem id et.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(11, 'Rerum maiores et eligendi et maiores.', 'Consequatur ducimus est eos laborum dolorem. Nisi est debitis magni numquam omnis. Neque esse perspiciatis rerum ut.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(12, 'Voluptatum aut consequatur enim occaecati.', 'Laborum omnis aut enim at ex. Sint aliquid numquam est exercitationem iste ut. Enim tenetur ipsum maiores alias fugit ipsam. Qui omnis rerum ipsam officia esse cupiditate officiis quaerat.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(13, 'Rerum facilis et id omnis.2323', 'Assumenda et sed pariatur. Laudantium quia repudiandae et maiores. Quaerat reru123123m qui ut. Ut dolor pariatur at earum delectus ut.', '2018-09-08 10:14:43', '2018-09-09 19:17:39'),
(14, 'Vel sit quo aliquid.', 'Aliquam et illo modi magni et. Quia consequuntur et autem magnam corrupti ex velit. Enim nobis saepe provident minus.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(15, 'Enim quaerat quia incidunt ut quia delectus.', 'Est veniam iure dignissimos ut eos sit tempora. Voluptates autem optio consequatur id praesentium non. Nostrum ex voluptas quo qui mollitia dolorem.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(16, 'Blanditiis veniam enim ut quo nemo vel.', 'Dolores architecto dolor est veniam. Sunt ipsam et reiciendis. Asperiores voluptas odio voluptatibus aut praesentium mollitia.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(17, 'Fuga enim quasi velit qui nisi voluptatum quae.', 'Autem asperiores voluptatem sapiente et. Ut tempore asperiores eaque molestiae. Est voluptate sint nesciunt praesentium modi aut vel.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(18, 'Dolorem eveniet blanditiis assumenda laudantium.', 'Quis repellendus cumque molestiae. Aut rerum ipsam aut voluptas provident ut. Molestiae ipsam sed ad rerum.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(19, 'Cumque sit deleniti est nesciunt unde qui.', 'Ea culpa alias sit rem repellat voluptatem. Quibusdam velit minima quibusdam minima accusamus sed. Consequatur ut ut ullam est facere facilis saepe. Tenetur illo consequatur iusto quod.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(20, 'Cumque non ut sed velit.', 'Quia suscipit qui quo placeat. Et aperiam libero consequatur voluptas quia quaerat. Quo sunt numquam quam molestias accusantium.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(21, 'Non ullam eos ut et nobis quis earum.', 'Sed id commodi ut. Voluptatum numquam voluptatem et cumque dignissimos deserunt nihil. Enim voluptates est laudantium aut atque doloribus odio. Non ullam et perferendis laborum cumque.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(22, 'Ut sit ullam architecto debitis.', 'Repellendus qui alias numquam corrupti vel at tempore. Amet molestias fugit aut. Cumque aut a aut et atque. Adipisci velit corrupti quia ducimus.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(23, 'Assumenda soluta modi provident fuga est.', 'Asperiores mollitia nostrum illo porro possimus doloribus exercitationem officiis. Itaque ex omnis minus maxime. Facere asperiores nisi incidunt ipsam consectetur dolore pariatur.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(24, 'Assumenda odit et ea quam.', 'Eum harum nobis voluptas ipsa. Voluptatum pariatur laudantium corrupti quaerat necessitatibus voluptatem ut. Error non quae aut voluptate asperiores dolorum.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(25, 'Ea ipsam vitae error harum ea.', 'Neque et est quia dolorem. Esse dolor distinctio ut suscipit. Qui ut error maiores aliquam vero facere omnis. Sed impedit sed cupiditate voluptate repellendus magni natus.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(26, 'Eaque quia autem nihil maiores aut modi.', 'Cum et ab aliquam atque. Minima omnis nobis possimus eum fuga est. Incidunt expedita doloremque rerum dolor aut. Perspiciatis amet est non accusamus.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(27, 'Facilis mollitia minus omnis adipisci culpa quae.', 'Minus reprehenderit beatae dolor dolorem aut odit. Quae dolorem sequi mollitia quia. Possimus qui illum quia aut consequatur fuga aliquid voluptate. Sunt ea et voluptas sit accusamus non.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(28, 'Ad qui sunt aperiam quaerat quia vel eius.', 'Nobis at laudantium veniam sit rerum praesentium aut. Voluptatem esse consequatur aut quis deleniti dolor. Ut modi nemo eos quia consequatur. Deleniti delectus et exercitationem nam sint facere.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(29, 'Doloribus hic ducimus nemo error illo nisi totam.', 'Est maiores ea qui ut. Omnis quia tempora at corporis. Excepturi aspernatur excepturi voluptas quisquam. Aliquid fugiat dolorum nihil officia et omnis.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(30, 'Autem in occaecati est vitae officiis.', 'Iste ratione sint voluptatem ipsum consequuntur accusantium qui voluptates. Dolorem aspernatur rem quia provident aut et sit quam. Eligendi et est aut quia nemo. Animi quam voluptas ipsum enim.', '2018-09-08 10:14:43', '2018-09-08 10:14:43'),
(35, 'test title21123123123123123123', 'test body', '2018-09-10 16:59:09', '2018-09-10 16:59:09');

-- --------------------------------------------------------

--
-- Структура таблицы `article_images`
--

CREATE TABLE `article_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_08_125004_create_articles_table', 1),
(4, '2018_09_09_224637_create_article_images_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_images`
--
ALTER TABLE `article_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `article_images`
--
ALTER TABLE `article_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
