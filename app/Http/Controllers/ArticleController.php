<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Image;
use App\Article;
use App\ArticleImages;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\ArticleImages as ArticleImagesResource;

class ArticleController extends Controller
{
    public function index()
    {
       // Get articles
       $articles = Article::orderBy('created_at', 'desc')->paginate(6);
       foreach($articles as &$article){
        $article->image_path = ArticleImagesResource::collection(ArticleImages::where('article_id', '=', $article->id)->get());
       }
         

       // Return collection of articles
       return ArticleResource::collection($articles);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $article = $request->isMethod('put') ? 
            Article::findOrFail($request->article_id) : 
            new Article;
        $article->id = $request->input('article_id');
        $article->title = $request->input('title');
        $article->body = $request->input('body');
        $article_images = $_FILES;
        
        if($article->save()) {

            foreach($article_images as $file){
                $article_image = new ArticleImages;

                $png_url = "product-".time().".png";
                $path = public_path('./img/' . $png_url);
        
                Image::make($file->file('image')->getRealPath())->save($path);

                $article_image->image_path = $path;
                $article_image->article_id = $article->id;
                $article_image->save();
           
            }

            return new ArticleResource($article);
        }
    }

    public function show($id)
    {
        // Get article
        $article = Article::findOrFail($id);

        return new ArticleResource($article);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        
        if($article->delete()) {
            return new ArticleResource($article);
        }
        
    }
}
